The point of this experiment is to make English phonetically
consistent without changing the alphabet.

## Basic idea

The alphabet is left unchanged and the pronounciation is changed
instead. Every word is pronounciation becomes what it'd be if you just
used its spelling as an IPA transcription.

## Letters

| Letter | Sound |
| ------ | ----- |
| a      | a     |
| b      | b     |
| c      | c     |
| d      | d     |
| e      | e     |
| f      | f     |
| g      | g     |
| h      | h     |
| i      | i     |
| j      | j     |
| k      | k     |
| l      | l     |
| m      | m     |
| n      | n     |
| o      | o     |
| p      | p     |
| q      | q     |
| r      | r     |
| s      | s     |
| t      | t     |
| u      | u     |
| v      | v     |
| w      | w     |
| x      | x     |
| y      | y     |
| z      | z     |

## Letter name pronunciation

The same rule is applied as to the rest of the English words. For
example, the name of the letter "h" is "aitch," so it's pronounced
/aitch/.

## Example text

The quick brown fox jumps over the lazy dog.

/the quick brown fox jumps over the lazy dog/
