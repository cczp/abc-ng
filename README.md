The point of this experiment is to expand the English alphabet with
additional letters, simplifying it in the process and allowing it to
be phonetically consistent.

## Basic rules

* Everything is spelled (almost) exactly as it sounds like.
* Stressed General American pronunciation of individual words is
  assumed.

## Letters

There are no uppercase letters.

| Letter | Sounds    |
| ------ | --------- |
| a      | a, ʌ      |
| ɑ      | ɑ         |
| æ      | æ         |
| b      | b         |
| c      | k         |
| d      | d         |
| ð      | ð         |
| e      | ɛ, e, ə\* |
| f      | f         |
| g      | g         |
| h      | h         |
| i      | i, j      |
| j      | ʒ         |
| l      | l         |
| m      | m         |
| n      | n         |
| ŋ      | ŋ         |
| o      | ɔ, o      |
| œ      | ɜ\*\*     |
| p      | p         |
| r      | ɹ         |
| s      | s         |
| ʃ      | ʃ\*\*\*   |
| t      | t         |
| þ      | θ         |
| u      | u, ʊ      |
| v      | v         |
| w      | w         |
| y      | ɪ         |
| z      | z         |


\*Nothing is written if the /ə/ is between 2 consonants.

\*\*Yes, I know, it doesn't make any sense, just trust me.

\*\*\*/tʃ/ is treated as 2 separate sounds.

## Letter name pronunciation

* If it's a vowel - *its sound*
* If it's a consonant - *its sound* + ʌ

## Example text

æn ostrelyen fyʃ wyð æn ympresyvli lɑrdj vdjayne yz tʃoucyŋ tu deþ e
bœrd neymd helge.

An Australian fish with an impressively large vagina is choking to
death a bird named Helga.
